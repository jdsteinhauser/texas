use Mix.Config

selenium_host =
  if (System.get_env("CI") == "true") do
    "selenium"
  else
    "localhost"
  end

web_host =
  if (System.get_env("CI") == "true") do
    System.get_env("CONTAINER_IP")
  else
    "localhost"
  end

config :hound,
  driver: "chrome_driver",
  host: selenium_host,
  port: 4444,
  path_prefix: "wd/hub/"

config :texas, web_host: web_host

config :phoenix, :template_engines,
  tex:  Texas.TemplateEngine
config :texas, pubsub: WorkingOnDocsWeb.Endpoint
config :texas, router: WorkingOnDocsWeb.Router

config :working_on_docs, WorkingOnDocsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "7Uj0jfZkXrI/oVNyf9j059OMGhoHpyucu5Qbw2bVWwJFYz6Ufm7Wru/ADbtvIRVc",
  render_errors: [view: WorkingOnDocsWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: WorkingOnDocs.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :working_on_docs, WorkingOnDocsWeb.Endpoint,
  http: [port: 4000],
  server: true

# Print only warnings and errors during test
config :logger, level: :warn
