[![badge](https://img.shields.io/badge/Texas-v0.3.2-blue.svg?style=for-the-badge&label=Texas)](https://hex.pm/packages/texas/0.3.2)
[![badge](https://img.shields.io/badge/texasjs-v0.3.19-blue.svg?style=for-the-badge&label=TexasJS)](https://www.npmjs.com/package/texasjs/v/0.3.19)

# What is Texas?

Texas is a back-end Virtual DOM library.  It aims to overhaul how you work with phoenix's view layer so that you can spend more time developing on the server, rather than dealing with complex toolchains and build systems that offload work to the client.  The main goal for Texas is to create a library that significantly speeds up development and testing, but I believe this library can achieve many advancements over how we've been writing web apps, including: reduced data over the wire, faster response times, and graceful degredation to an application that continues to function in the abscence of any JS.

##### This project builds on top of [Phoenix](http://phoenixframework.org/), so you'll need a new phoenix project.  These install docs assume you're using phoenix 1.3+.
#### here's a talk I gave at elixirconf 2018 about the high level goals and runtime:

[<img src="http://i3.ytimg.com/vi/NWgwUKfR8Xg/maxresdefault.jpg"  width="420" height="240">](https://www.youtube.com/watch?v=NWgwUKfR8Xg)

#### You can find an example application [here](https://gitlab.com/dgmcguire/example_texas_app).
#### The example application live: https://warm-citadel-23442.herokuapp.com/ (open the app in multiple sessions, all operations sync with all clients in realtime)

#### Here's a getting started series that's slowly getting some love 
[<img src="http://i3.ytimg.com/vi/LHPaWqnDcBU/maxresdefault.jpg"  width="420" height="240">](https://www.youtube.com/watch?v=LHPaWqnDcBU&list=PLpie8aMMh6cfsaLy-Hnpr3SL2pM6Mwe15)


# Installation

## 1. Add Texas to your dependencies
```elixir
# file: ./mix.exs
  defp deps do
    [
      ...
      {:texas, "X.X.X"}, # latest should be denoted by the Texas hex badge at the top
      ...
    ]
  end
```
```elixir
# file: ./assets/package.json
  ...
  "dependencies": {
    ...
    "texasjs": "X.X.X", # latest should be denoted by the texasjs npm badge at the top
    ...
  },
```

After updating your dependencies, install them:
 - $ `mix deps.get`
 - $ `cd assets/ && npm install`

## 2. Lift your Phoenix project to include Texas

Run `mix texas.ify` to add Texas hooks into the current Phoenix application (assumed to be create with `mix phx.new`).
  This will modify the following files as described, so it is wise to commit your changes prior to
  running this command:
  - `./assets/js/app.js` - (adds `texasjs` dependency and instantiates a new Texas socket)
  - `./lib/<your_app>_web/channels/user_socket.ex` - adds `use Texas.Socket` after `use Phoenix.Socket`
  - `./config/config.exs` -  Specifies endpoint and router modules and configures templating engine to compile .tex files
  - `./lib/<your_app>_web/router.ex` - Adds the `Texas.Plug` to your browser pipeline
  - `./lib/<your_app>_web.ex/` - imports `Texas.Controller` into your app's controller template

## 3. Creating a dynamic template and view
> NOTE: The template needs to be `.tex` (or whatever you put in your config) to get run thru the texas template engine!

```html
# file: ./lib/<your_app>_web/templates/page/index.html.tex

<div data-texas="example_list">
  Some example prototype data that will be overwritten.
</div>
```

> NOTE: With a default Phoenix install, the <your_view> placeholder should be `./lib/<your_app>_web/templates/page/index.html.eex`.
> We'll be changing the `index.html.eex` file to `index.html.tex`

```elixir
# file: ./lib/<your_app>_web/views/page_view.ex

defmodule <YourApp>Web.PageView do
  use <YourApp>Web, :view

  def data(conn) do
    %{ example_list: example_list(conn) }
  end

  def example_list(_conn) do
    ~s|
      <div class="some-class">
        #{example_text_node()}
      </div>
    | 
    # ultimately texas needs AST, not html string
    # so we parse it out here for now 
    # (future versions will clean this up so you can just worry about html strings)
    |> Floki.parse()
  end

  defp example_text_node, do: "<div> see it's overwritten! </div>"
end
```

## 4. Responding with that rendered view via a controller
```elixir
# file: ./lib/<your_app>_web/controllers/page_controller.ex

defmodule <YourApp>Web.PageController do

  def index(conn, _params) do
    texas_render conn, "index.html", [texas: <YourApp>Web.PageView.data(conn)]
  end
end
```

## 5. Building Texas templates in your favorite editor
Since Texas's TEX files are just plain HTML with some `data-texas` attributes, you can associate *.tex files in your favorite editor with the HTML renderer! For example, to add this association in VS Code, open your User Preferences and add:
```javascript
// Place your settings in this file to overwrite the default settings
{
    ...
    "files.associations": { "*.tex" : "html" },
    ...
}
```