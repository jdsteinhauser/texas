defmodule Texas.Mixfile do
  use Mix.Project

  def project do
    [app: :texas,
     version: "0.3.2",
     elixir: "~> 1.5",
     elixirc_paths: elixirc_paths(Mix.env),
     build_embedded: Mix.env == :prod,
     compilers: [:phoenix] ++ Mix.compilers,
     start_permanent: Mix.env == :prod,
     description: description(),
     package: package(),
     deps: deps()]
  end

  def application do
    [ mod: {Texas.Application, []}, extra_applications: [:logger] ]
  end

  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:floki, "~>0.17.0"},
      {:phoenix, "~>1.3"},
      {:phoenix_html, "~>2.10"},
      {:uuid, "~>1.1"},
      {:hound, "~> 1.0.0", only: [:test]},
      {:gettext, "~> 0.11", only: [:test]},
      {:cowboy, "~> 1.0", only: [:test]},
      {:html_sanitize_ex, "~> 1.3.0-rc3", only: [:test]},
    ]
  end

  defp description do
    "Does anyone actually read this?"
  end

  defp elixirc_paths(:test), do: ["lib", "test/support", "test/support/working_on_docs"]
  defp elixirc_paths(_), do: ["lib"]

  defp package do
    [
      name: :texas,
      files: ~W(lib mix.exs README.md),
      maintainers: ["Dan McGuire"],
      licenses: ["MIT"],
      links: %{"Gitlab" => "https://gitlab.com/dgmcguire/texas"},
    ]
  end
end
