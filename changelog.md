# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.3] [Unreleased]
### Changed
- Changed: Client modules renamed to Cache
- Feature: CI pipeline is now working although no real test coverage has been implemented yet
- Fixed: websocket reconnects will cause a page refresh if the cache is out of sync [issue 25](https://gitlab.com/dgmcguire/texas/issues/25)
- Fixed: html comments no longer crash the templating engine [issue 17](https://gitlab.com/dgmcguire/texas/issues/17)
- Fixed: html methods coming over a socket no longer break if you have the wrong capitalization [issue 20](https://gitlab.com/dgmcguire/texas/issues/20)
- New: mix task to inject all the neccesary setup/config